# Powerball Site
This library contains all the functionality required to talk to the [Chaos Powerball League] website using the web API. Library is written in c++ and requires you to link to `libcurl.lib`.

### Dependencies and Licenses
This project depends on:
 - [rapidjson]
 - [libcurl]

See **licenses.txt** for information regarding the licensing of external libraries used in this project.

### Usage
Build the project and link to `powerballsite.lib`.

[Chaos Powerball League]:http://powerball.svssubspace.com
[rapidjson]:https://github.com/miloyip/rapidjson
[libcurl]:http://curl.haxx.se/libcurl/