#include "stdafx.h"

#include "auth.h"
#include "sha2.h"

/* Wrapper around sha256 that returns a std::string */
std::string Authenticator::getSHA256Digest(std::string message) {
	unsigned char digest[SHA256_DIGEST_SIZE];
	sha256(reinterpret_cast<const unsigned char*>(message.c_str()), message.length(), digest);

	std::stringstream digestString;
	for (int i = 0; i < SHA256_DIGEST_SIZE; i++) {
		digestString << static_cast<char>(digest[i]);
	}
	return digestString.str();
}

#define CHARLOWORD(x) (x & 0x0F)
#define CHARHIWORD(x) ((x >> 4) & 0x0F)
std::string stringToHex(std::string input) {
	std::stringstream hexdigest;

	int i = 0;
	for (std::string::iterator iter = input.begin(); iter != input.end(); iter++) {
		unsigned char value = static_cast<unsigned char>(*iter);

		hexdigest << std::hex << CHARHIWORD(value);
		hexdigest << std::hex << CHARLOWORD(value);
		i++;
	}

	return std::string(hexdigest.str());
}

/* Loads the secret key from the secret.txt file and zero pads it to the block size length */
void Authenticator::loadSecretKey() {
	// Get the secret key from secret.txt
	std::string secret;

	std::ifstream secretFile("secret.txt", std::ifstream::in);

	if (secretFile.good()) {
		std::getline(secretFile, secret);
	}
	else {
		throw std::runtime_error("Could not load secret key from secret key file.");
	}

	if (secret.length() > SHA256_BLOCK_SIZE) {
		secret = getSHA256Digest(secret);
	}

	std::stringstream paddedKey;
	paddedKey << std::left << std::setw(SHA256_BLOCK_SIZE) << std::setfill('\0') << secret;
	key = paddedKey.str();

	/* Compute special values for hmac */
	std::stringstream oPad;
	std::stringstream iPad;
	for (int i = 0; i < SHA256_BLOCK_SIZE; i++) {
		oPad << static_cast<char>(key.at(i) ^ 0x5C);
		iPad << static_cast<char>(key.at(i) ^ 0x36);
	}

	iKeyPad = iPad.str();
	oKeyPad = oPad.str();
}

Authenticator::Authenticator() {
	loadSecretKey();
}

std::string Authenticator::createToken(std::string message)
{
	// calculate hmac-sha25
	std::string i(iKeyPad); // append modifys string (doesn't return a copy)
	std::string o(oKeyPad);
	return stringToHex(getSHA256Digest(o.append(getSHA256Digest(i.append(message)))));
}
