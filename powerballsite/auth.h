class Authenticator {
private:
	std::string key;
	std::string oKeyPad;
	std::string iKeyPad;

	std::string getSHA256Digest(std::string message);
	void loadSecretKey();
public:
	Authenticator();
	std::string createToken(std::string data);
};