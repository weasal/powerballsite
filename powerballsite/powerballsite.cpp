#include "stdafx.h"

#include "powerballsite.h"
#include "auth.h"

/* Site settings */
#ifdef _DEBUG
#define HOST "http://localhost:3000/"
#else
#define HOST "http://sspowerball.herokuapp.com/"
#endif

const std::unordered_map<PowerballSite::SiteAction, std::string> PowerballSite::ACTION_URIS = {
		{ PowerballSite::SiteAction::Verify, HOST "bot/verify" },
		{ PowerballSite::SiteAction::PilotInfo, HOST "bot/pilotinfo" },
		{ PowerballSite::SiteAction::ReportGame, HOST "bot/reportgame" },
		{ PowerballSite::SiteAction::BalanceTeams, HOST "bot/balanceteams" }
};

class PowerballSite::HiddenMembers {
public:
	Authenticator auth;
	CURL* curlHandle;

	HiddenMembers() {
	}
};

std::string PowerballSite::getURL(SiteAction action) {
	return PowerballSite::ACTION_URIS.at(action);
}

PowerballSite::PowerballSite() : members{ new HiddenMembers() } {
	members->curlHandle = curl_easy_init();
}

PowerballSite::~PowerballSite() {
	curl_easy_cleanup(members->curlHandle);
}

/** Sends a post request to url with json data */
void PowerballSite::sendPostRequest(std::string url, std::string data, std::function<void (bool, int, std::string)> callback) {
	curl_easy_reset(members->curlHandle);

	curl_slist* customHeaders = nullptr;
	customHeaders = curl_slist_append(customHeaders, "Content-Type: application/json");

	std::string authHeader = "Authorization: Token token=";
	authHeader.append(members->auth.createToken(data));
	customHeaders = curl_slist_append(customHeaders, authHeader.c_str());

	curl_easy_setopt(members->curlHandle, CURLOPT_HTTPHEADER, customHeaders);

	curl_easy_setopt(members->curlHandle, CURLOPT_POSTFIELDS, data.c_str());
	curl_easy_setopt(members->curlHandle, CURLOPT_URL, url.c_str());

	std::string response;
	curl_easy_setopt(members->curlHandle, CURLOPT_WRITEDATA, &response);

	// Since libcurl is a c library, need to use c-style callbacks (no lambda functions with captures)
	typedef size_t(*CURL_WRITEFUNCTION_PTR)(void*, size_t, size_t, void*);
	curl_easy_setopt(members->curlHandle, CURLOPT_WRITEFUNCTION, static_cast<CURL_WRITEFUNCTION_PTR>(
		[](void *contents, size_t size, size_t nmemb, void *userdata) {
		std::string* writedata = static_cast<std::string*>(userdata);
		writedata->append(reinterpret_cast<char*>(contents), size * nmemb);
		return static_cast<size_t>(size*nmemb);
	}));

	CURLcode res = curl_easy_perform(members->curlHandle);
	if (res != CURLE_OK) {
		callback(false, 0, response);
	}
	else {
		int statusCode = 0;
		curl_easy_getinfo(members->curlHandle, CURLINFO_RESPONSE_CODE, &statusCode);
		callback(true, statusCode, response);
	}

	curl_slist_free_all(customHeaders);
}

void PowerballSite::sendGetRequest(std::string url, std::function<void (bool, int, std::string)> callback) {
	curl_easy_reset(members->curlHandle);

	curl_easy_setopt(members->curlHandle, CURLOPT_URL, url.c_str());

	std::string response;
	curl_easy_setopt(members->curlHandle, CURLOPT_WRITEDATA, &response);

	// Since libcurl is a c library, need to use c-style callbacks (no lambda functions with captures)
	typedef size_t(*CURL_WRITEFUNCTION_PTR)(void*, size_t, size_t, void*);
	curl_easy_setopt(members->curlHandle, CURLOPT_WRITEFUNCTION, static_cast<CURL_WRITEFUNCTION_PTR>(
		[](void *contents, size_t size, size_t nmemb, void *userdata) {
		std::string* writedata = static_cast<std::string*>(userdata);
		writedata->append(reinterpret_cast<char*>(contents), size * nmemb);
		return static_cast<size_t>(size*nmemb);
	}));

	CURLcode res = curl_easy_perform(members->curlHandle);
	if (res != CURLE_OK) {
		callback(false, 0, response);
	}
	else {
		int statusCode = 0;
		curl_easy_getinfo(members->curlHandle, CURLINFO_RESPONSE_CODE, &statusCode);
		callback(true, statusCode, response);
	}
}

/**
"curl -H \"Authorization: Token token=<token>\"-d name=#{@name} -d code=#{@confirm_code} http://localhost:3000/registers/verify"
*/
bool PowerballSite::verifyPilot(std::string name, std::string confirmationCode) {
	rapidjson::Document verifyDoc;
	verifyDoc.SetObject();

	rapidjson::Value nameVal;
	nameVal = rapidjson::StringRef(name.c_str());
	verifyDoc.AddMember("name", nameVal, verifyDoc.GetAllocator());

	rapidjson::Value code;
	code = rapidjson::StringRef(confirmationCode.c_str());
	verifyDoc.AddMember("code", code, verifyDoc.GetAllocator());
	
	rapidjson::StringBuffer buffer;
	rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
	verifyDoc.Accept(writer);

	bool confirmationSuccess = false;
	sendPostRequest(getURL(PowerballSite::SiteAction::Verify), buffer.GetString(),
		[&confirmationSuccess](bool succeeded, int status, std::string response) {
		if (succeeded) 
			confirmationSuccess = (status == 201);
		}
	);

	return confirmationSuccess;
}

std::string timeToISO8601(const std::chrono::steady_clock::time_point& t) {
	using namespace std::chrono;
	const time_t ct
		((duration_cast<seconds>(system_clock::now().time_since_epoch())
		+ duration_cast<seconds>(t - steady_clock::now())
		).count()
		);
	std::string r = "0000-00-00T00:00:00Z.";
	struct tm newtime;
	gmtime_s(&newtime, &ct);
	std::strftime(const_cast<char*>(r.data()), r.size(), "%Y-%m-%dT%H:%M:%SZ", &newtime);
	return r;
}

/**
	Returns the game-id if successfully created a game on site (http://localhost:3000/games/<game-id>
*/
int PowerballSite::reportGame(GameReport game, std::string mapName, bool isMatch, int matchId) {
	using namespace rapidjson;

	Document reportDoc;
	reportDoc.SetObject();

	Document::AllocatorType& allocator = reportDoc.GetAllocator();

	if (isMatch)
		reportDoc.AddMember("matchId", Value(matchId), allocator);

	// For rapidjson -- ignore null terminator in length
	reportDoc.AddMember("map", StringRef(mapName.c_str()), allocator);
	std::string startedAtStr = timeToISO8601(game.startedAt);
	reportDoc.AddMember("startedAt", StringRef(startedAtStr.c_str()), allocator);

	Value scoreObject;
	scoreObject.SetObject();
	scoreObject.AddMember("warbirds", Value(game.score.warbirds), allocator);
	scoreObject.AddMember("javelins", Value(game.score.javelins), allocator);
	reportDoc.AddMember("score", scoreObject, allocator);

	reportDoc.AddMember("length", Value(game.length), allocator);

	Value goalsArray;
	goalsArray.SetArray();
	for (LoggedGoal goal : game.goals) {
		Value goalObject;
		goalObject.SetObject();

		Value teamVal;
		teamVal.SetString(goal.team.c_str(), allocator);
		goalObject.AddMember("team", teamVal, allocator);
		Value scorerVal;
		scorerVal.SetString(goal.scorer.c_str(), allocator);
		goalObject.AddMember("scorer", scorerVal, allocator);
		Value assisterVal;
		assisterVal.SetString(goal.assister.c_str(), allocator);
		goalObject.AddMember("assister", assisterVal, allocator);
		Value scoredAtVal;
		scoredAtVal.SetString(timeToISO8601(goal.scoredAt).c_str(), allocator);
		goalObject.AddMember("scoredAt", scoredAtVal, allocator);
		Value shotAtVal;
		shotAtVal.SetString(timeToISO8601(goal.shotAt).c_str(), allocator);
		goalObject.AddMember("shotAt", shotAtVal, allocator);

		Value trajectoryArray;
		trajectoryArray.SetArray();
		for (auto coord : goal.trajectory) {
			Value coordObject;
			coordObject.SetObject();
			coordObject.AddMember("x", Value(coord.first), allocator);
			coordObject.AddMember("y", Value(coord.second), allocator);
			trajectoryArray.PushBack(coordObject, allocator);
		}
		goalObject.AddMember("trajectory", trajectoryArray, allocator);

		Value goalieArray;
		goalieArray.SetArray();
		for (std::string goalie : goal.scoredOn) {
			Value goalieNameVal;
			goalieNameVal.SetString(goalie.c_str(), allocator);
			goalieArray.PushBack(goalieNameVal, allocator);
		}
		goalObject.AddMember("scoredOn", goalieArray, allocator);

		goalObject.AddMember("heldFor", Value(static_cast<int>(goal.heldFor.count())), allocator);

		goalsArray.PushBack(goalObject, allocator);
	}

	reportDoc.AddMember("goals", goalsArray, allocator);

	Value participantsArray;
	participantsArray.SetArray();
	for (LoggedParticipant participant : game.participants) {
		Value participantObj;
		participantObj.SetObject();

		Value nameVal;
		nameVal.SetString(participant.name.c_str(), allocator);
		participantObj.AddMember("name", nameVal, allocator);

		Value teamVal;
		teamVal.SetString(participant.team.c_str(), allocator);
		participantObj.AddMember("team", teamVal, allocator);

		Value playtimeVal(participant.playtime);
		participantObj.AddMember("playtime", playtimeVal, allocator);

		// Note rails does not support arrays of arrays in params
		Value activeArray;
		activeArray.SetArray();
		for (auto interval : participant.activeTimes) {
			Value intervalObject;
			intervalObject.SetObject();
			Value startVal;
			startVal.SetString(timeToISO8601(interval.first).c_str(), allocator);
			intervalObject.AddMember("start", startVal, allocator);
			Value endVal;
			endVal.SetString(timeToISO8601(interval.second).c_str(), allocator);
			intervalObject.AddMember("end", endVal, allocator);
			activeArray.PushBack(intervalObject, allocator);
		}

		participantObj.AddMember("activeTimes", activeArray, allocator);

		Value statsObj;
		statsObj.SetObject();
		statsObj.AddMember("goals", Value(participant.stats.goals), allocator);
		statsObj.AddMember("assists", Value(participant.stats.assists), allocator);
		statsObj.AddMember("saves", Value(participant.stats.saves), allocator);
		statsObj.AddMember("goalsallowed", Value(participant.stats.goalsallowed), allocator);
		statsObj.AddMember("ballcarries", Value(participant.stats.ballcarries), allocator);
		statsObj.AddMember("steals", Value(participant.stats.steals), allocator);
		statsObj.AddMember("turnovers", Value(participant.stats.turnovers), allocator);
		statsObj.AddMember("pressures", Value(participant.stats.pressures), allocator);
		statsObj.AddMember("kills", Value(participant.stats.kills), allocator);
		statsObj.AddMember("deaths", Value(participant.stats.deaths), allocator);
		participantObj.AddMember("stats", statsObj, allocator);

		participantsArray.PushBack(participantObj, allocator);
	}

	reportDoc.AddMember("participants", participantsArray, allocator);

	StringBuffer buffer;
	Writer<StringBuffer> writer(buffer);
	reportDoc.Accept(writer);

	int gameId;
	sendPostRequest(getURL(PowerballSite::SiteAction::ReportGame), buffer.GetString(),
		[&gameId](bool succeeded, int status, std::string response) {
		if (succeeded) {
			Document document;
			document.Parse(response.c_str());
			if (document.IsObject() && document.HasMember("id")) {
				gameId = document["id"].GetInt();
			}
		}
		else {
			gameId = -1;
		}
	});

	return gameId;
}

#define PILOT_NOT_FOUND 404
bool PowerballSite::pilotIsRegistered(std::string name) {
	bool registered = false;
	std::string infoURL = getURL(SiteAction::PilotInfo);
	infoURL.append("?name=");
	std::string escapedName = curl_easy_escape(members->curlHandle, name.c_str(), name.length());
	infoURL.append(escapedName);
	sendGetRequest(infoURL, [&registered](bool success, int status, std::string response) {
		if (success && status == 200) {
			registered = true;
		}
	});

	return registered;
}

std::string PowerballSite::getPilotSquad(std::string name) {
	using namespace rapidjson;

	std::string squad;
	std::string infoURL = getURL(SiteAction::PilotInfo);
	infoURL.append("/");
	infoURL.append(name);
	sendGetRequest(infoURL, [&squad](bool success, int status, std::string response) {
		if (success && status != PILOT_NOT_FOUND) {
			Document document;
			document.Parse(response.c_str());
			if (document.IsObject() && document.HasMember("squad")) {
				squad = document["squad"].GetString();
			}
		}
	});

	return squad;
}

std::array<std::vector<std::string>, 2> PowerballSite::getBalancedTeams(std::vector<std::string> players) {
	using namespace rapidjson;

	Document playersDoc;
	playersDoc.SetArray();

	for (auto playerName : players) {
		Value nameVal;
		nameVal.SetString(playerName.c_str(), playersDoc.GetAllocator());
		playersDoc.PushBack(nameVal, playersDoc.GetAllocator());
	}

	StringBuffer buffer;
	Writer<rapidjson::StringBuffer> writer(buffer);
	playersDoc.Accept(writer);

	std::string listJson = buffer.GetString();
	std::string escapedList = curl_easy_escape(members->curlHandle, listJson.c_str(), listJson.length());

	std::string balanceURL = getURL(SiteAction::BalanceTeams);
	balanceURL.append("?players=");
	balanceURL.append(escapedList);

	std::vector<std::string> warbirds, javelins;
	sendGetRequest(balanceURL, [&warbirds, &javelins](bool success, int status, std::string response) {
		if (success) {
			Document document;
			document.Parse(response.c_str());

			if (document.IsObject() && document.HasMember("warbirds")) {
				const Value& warbirdsArray = document["warbirds"];
				if (warbirdsArray.IsArray()) {
					for (Value::ConstValueIterator itr = warbirdsArray.Begin(); itr != warbirdsArray.End(); ++itr) {
						warbirds.push_back(itr->GetString());
					}
				}
			}

			if (document.IsObject() && document.HasMember("javelins")) {
				const Value& javelinsArray = document["javelins"];
				if (javelinsArray.IsArray()) {
					for (Value::ConstValueIterator itr = javelinsArray.Begin(); itr != javelinsArray.End(); ++itr) {
						javelins.push_back(itr->GetString());
					}
				}
			}
		}
	});

	return {{ warbirds, javelins }};
}