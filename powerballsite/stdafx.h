#pragma once

#include "targetver.h"

#include <string>
#include <iostream>
#include <ctime> // for strftime in ISO860 conversion function
#include <array>
#include <unordered_map>
#include <memory>
#include <functional>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <exception>

#define CURL_STATICLIB
#include "curl\curl.h"

#include "rapidjson\document.h"
#include "rapidjson\writer.h"
#include "rapidjson\stringbuffer.h"